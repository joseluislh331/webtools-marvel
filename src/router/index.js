import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: () => import('../views/Detail.vue')
    // comprobar si tiene id, si no, le mando a la home
  },
  { path: "*",
    component: Home
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
