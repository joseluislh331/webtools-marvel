import axios from "axios";
import md5 from "js-md5";

export default {
    data: () => ({
        publickey: "c2b682fe5d945f5e64ae46a2996df5bf",
        privatekey: "e076e26b198169354f95803da65f2de660327379"
    }),
    methods: {
        getComics(page, comicListLimit) {
            var ts = new Date().getTime();
            var stringToHash = ts + this.privatekey + this.publickey;
            var hash = md5(stringToHash);
            return axios
                .get(
                    `http://gateway.marvel.com/v1/public/comics?ts=${ts}&apikey=${this.publickey}&hash=${hash}&orderBy=title&limit=${comicListLimit}&offset=${(page * 50) - 50}`
                )
                .then(response => {
                    return response
                })
                .catch(error => {
                    console.warn("ERROR", error);
                });
        },
        getComicDetail(id) {
            var ts = new Date().getTime();
            var stringToHash = ts + this.privatekey + this.publickey;
            var hash = md5(stringToHash);
            return axios
                .get(
                    `http://gateway.marvel.com/v1/public/comics/${id}?ts=${ts}&apikey=${this.publickey}&hash=${hash}`
                )
                .then(response => {
                    return response;
                })
                .catch(error => {
                    console.warn("ERROR", error);
                });
        }
    }
}