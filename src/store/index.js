import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    favorites: [],
    page: 1
  },
  mutations: {
    SET_FAVORITES(state, favorites) {
      state.favorites = favorites;
    },
    SET_PAGE(state, page) {
      state.page = page;
    }
  },
  actions: {
    setFavorites({commit, state}, favorite) {
      let favorites = state.favorites;
      if (!favorite && localStorage.getItem("favorites")) {
        favorites = JSON.parse(localStorage.getItem("favorites"));
      } else if (favorite) {
        if (favorites && favorites.find(comic => comic.id === favorite.id))
          favorites = favorites.filter(comic => comic.id !== favorite.id);
        else 
          favorites.push(favorite);
        localStorage.setItem("favorites", JSON.stringify(favorites));
      }
      commit('SET_FAVORITES', favorites);
    },
    setPage({commit}, page) {
      commit('SET_PAGE', page);
    }
  }
})